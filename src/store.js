import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';

import rootReducer from './reducers';


// const localStorageMiddleware = ({ getState }) => {
//     return next => action => {
//         const result = next(action);
//         if ([ actions.addToCart ].includes(result.ADD_TO_CART)) {
//             localStorage.setItem(appConstants.APP_STATE, JSON.stringify(getState()))
//         }
//         return result;
//     };
// };

// const reHydrateStore = () => {
//     const data = localStorage.getItem(appConstants.APP_STATE);
//     if (data) {
//         return JSON.parse(data);
//     }
//     return undefined;
// };



// export default () => {
//   const store = createStore(rootReducer,reHydrateStore(), applyMiddleware(logger, localStorageMiddleware));
//   return store;
// };

export default () => {
  const store = createStore(rootReducer, applyMiddleware(logger));
  return store;
};
