import React from 'react';

import { Input, Menu, Icon, Button, Popup } from 'semantic-ui-react';

const Filter = ({ setFilter, filterBy }) => {

  return (<Menu secondary>
    <Menu.Item>
      </Menu.Item>
      <Button
        active={filterBy === 'all'}
        onClick={setFilter.bind(this, 'all')}>
        All
      </Button>
      <Button.Group size='large'>
          <Button 
            active={filterBy === 'price_high'}
            onClick={setFilter.bind(this, 'price_high')}>
            <Icon name="sort amount down" size="large"/>
          </Button>
        <Button.Or />
          <Button
            active={filterBy === 'price_low'}
            onClick={setFilter.bind(this, 'price_low')}>
            <Icon name="sort amount up" size="large"/>
          </Button>
        </Button.Group>
      <Button
        active={filterBy === 'author'}
        onClick={setFilter.bind(this, 'author')}
        icon="user"
        size="large"
        >
      </Button>
  </Menu>);


};

export default Filter;
