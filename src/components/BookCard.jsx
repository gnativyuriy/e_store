import React from 'react';
import { Card, Image, Icon, Button, Rating } from 'semantic-ui-react';
import { FaHryvnia } from "react-icons/fa";

const BookCard = book => {
  const { title, author, price, image, rating, addToCart, addedCount } = book;
  return (
    <Card>
      <div className="card-image">
        <Image src="https://react.semantic-ui.com/images/wireframe/image-text.png" />
      </div>
      <Card.Content>
        <Card.Header>{title}</Card.Header>
        <Card.Meta>
          <span className="date">{author}</span>
        </Card.Meta>
      </Card.Content>
      <Card.Content extra>
        <a>
          <FaHryvnia size="10px"/>
          {price}
        </a>
      </Card.Content>

      <Card.Content>
        <Rating icon='star' defaultRating={rating} maxRating={5} />
      </Card.Content>

      <Button 
        onClick={addToCart.bind(this, book)}
      >
        <Icon name="cart" size="large"/>
        {addedCount > 0 && `${addedCount}`}
      </Button>
    </Card>
  );
};

export default BookCard;

//create changing button icon after click
