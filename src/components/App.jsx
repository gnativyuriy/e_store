import React, { Component } from 'react';
import { Card, Container } from 'semantic-ui-react';
import axios from 'axios';
import useLocalStorage from "react-use-localstorage"
import bookPagination from "./Pagination"
import BookCard from '../containers/BookCard';
import Filter from '../containers/Filter';
import Menu from '../containers/Menu';
import { Pagination } from 'semantic-ui-react'


class App extends Component {

  componentWillMount() {
    const { setBooks } = this.props;
    axios.get('/books.json').then(({ data }) => {
      setBooks(data);
    });
  }
  
  render() {
    const { books, isReady } = this.props;
    return (
        <Container>
          <Menu />
          <Filter />
          <Card.Group itemsPerRow={3}>
            {!isReady
              ? 'loading...'
              : books.map((book, id) => <BookCard key={id} {...book} />)}
          </Card.Group>
        </Container>
    );
  }
}

export default App;
