import React from "react";
import { Menu, Popup, List, Button, Image, Icon, Input } from "semantic-ui-react";
import { FaHryvnia } from "react-icons/fa";
import { IoIosBook } from "react-icons/io";

const addStorage = () => {
  if (localStorage.total) {
    console.log('exists');
    return localStorage.getItem('total')
  }

  else {
    localStorage.setItem('total', 5000);
    return localStorage.getItem('total')
  }
}


const CartComponent = ({ title, id, image, removeFromCart }) => (
<List selection divided verticalAlign="middle">
    <List.Item>
      <List.Content floated="right">
        <Button onClick={removeFromCart.bind(this, id)} basic color='red'>
          Remove
        </Button>
      </List.Content>
        <Image avatar src="https://react.semantic-ui.com/images/wireframe/image-text.png" />
      <List.Content>{title}</List.Content>
    </List.Item>
  </List>
);
const MenuComponent = ({ totalPrice, count, items, searchQuery, setSearchQuery, clearCart }) => (
    <Menu>

      <Menu.Item name="browse">
        <IoIosBook size="30px"/>&nbsp;Book Store
      </Menu.Item>

      <Menu.Item>
        <Input
            icon="search"
            onChange={e => setSearchQuery(e.target.value)}
            value={searchQuery}
            placeholder="Enter name or author"
          />
      </Menu.Item>

      <Menu.Menu position="right">
        <Menu.Item name="signup">
          <Icon name="check circle" size="big"/>&nbsp; <b>{addStorage(totalPrice)} <FaHryvnia size="10px"/></b>&nbsp;
        </Menu.Item>
        
        <Popup
          trigger={
            <Menu.Item name="help">
              <Icon name="shopping cart" size="big"/><b>{count}</b>
            </Menu.Item>
          }
          content={
            items.map(book => (<CartComponent {...book} />))
          }
          on="click"
          hideOnScroll
          />
        <Menu.Item onClick={clearCart}><Icon name="delete" size="Big"/></Menu.Item>
        <Menu.Item onClick={addStorage}>{localStorage.getItem('total')}</Menu.Item>
      </Menu.Menu>
    </Menu>
);


export default MenuComponent;
