export const setSearchQuery = value => ({
  type: 'SET_QUERY',
  payload: value,
});

export const addToCart = obj => ({
  type: 'ADD_TO_CART',
  payload: obj,
});

export const removeFromCart = id => ({
  type: 'REMOVE_FROM_CART',
  payload: id,
});

export const clearCart = id => ({
  type: 'CLEAR_CART',
  payload: id,
});

